package advertiser

//go:generate mockery --name AdvertiserService
type AdvertiserService interface {
	GetAll() ([]Advertiser, error)
	GetByID(id int) (*Advertiser, error)
}

type Service struct {
	advertiserRepo AdvertiserRepository
}

func NewService(aRepo AdvertiserRepository) *Service {
	return &Service{
		advertiserRepo: aRepo,
	}
}

func (s Service) GetAll() ([]Advertiser, error) {
	data, err := s.advertiserRepo.GetAll()
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (s Service) GetByID(id int) (*Advertiser, error) {
	data, err := s.advertiserRepo.GetByID(id)
	if err != nil {
		return nil, err
	}

	return data, nil
}
