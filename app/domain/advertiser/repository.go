package advertiser

//go:generate mockery --name AdvertiserRepository
type AdvertiserRepository interface {
	GetAll() ([]Advertiser, error)
	GetByID(id int) (*Advertiser, error)
}
