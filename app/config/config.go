package config

import (
	"fmt"
	"github.com/ilyakaznacheev/cleanenv"
)

type Config struct {
	Server     Server
	Postgresql Postgresql
	Logger     Logger
}

func Load() (*Config, error) {
	cfg := Config{}

	if err := cleanenv.ReadConfig(".env", &cfg); err != nil {
		return nil, fmt.Errorf("parse env %v", err)
	}

	return &cfg, nil
}
