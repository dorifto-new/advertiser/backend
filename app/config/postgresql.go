package config

import "fmt"

type Postgresql struct {
	Host     string `env:"POSTGRESQL_HOST" env-required:"true"`
	Port     string `env:"POSTGRESQL_PORT" env-required:"true"`
	Database string `env:"POSTGRESQL_DATABASE" env-required:"true"`
	User     string `env:"POSTGRESQL_USER" env-required:"true"`
	Password string `env:"POSTGRESQL_PASSWORD" env-required:"true"`
}

func (p Postgresql) GetDsn() string {
	return fmt.Sprintf(
		"host=%s port=%s user=%s dbname=%s password=%s sslmode=disable",
		p.Host,
		p.Port,
		p.User,
		p.Database,
		p.Password,
	)
}
