package config

type Logger struct {
	Level int `env:"LOGGER_LEVEL" env-required:"true"`
}
