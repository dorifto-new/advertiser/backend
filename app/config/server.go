package config

type Server struct {
	Address string `env:"ADDRESS" env-required:"true"`
	Port    string `env:"PORT"    env-required:"true"`
}
