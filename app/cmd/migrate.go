package cmd

import (
	"advertiser/config"
	"advertiser/internal/databases"
	migrate "github.com/rubenv/sql-migrate"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var MigrateCmd = &cobra.Command{
	Use:   "migrate",
	Short: "Apply migrations",
	Run: func(cmd *cobra.Command, args []string) {
		cfg, err := config.Load()
		if err != nil {
			logrus.WithError(err).Error("failed load config")
		}

		logrus.SetLevel(logrus.Level(cfg.Logger.Level))

		db, err := databases.NewPostgresDB(cfg.Postgresql.GetDsn())
		if err != nil {
			logrus.WithError(err).Error("postgresql db connection failed")
		}
		defer db.Close()

		res, err := migrate.Exec(db.DB, "postgres", &migrate.FileMigrationSource{Dir: "migrations/postgresql"}, migrate.Up)
		if err != nil {
			logrus.WithError(err).Error("failed apply postgres migrations")
		}

		logrus.Infof("Successfully applied %d migrations", res)
	},
}
