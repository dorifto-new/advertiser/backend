package cmd

import (
	"advertiser/config"
	"advertiser/internal/api/rest"
	"advertiser/internal/databases"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var RunCmd = &cobra.Command{
	Use:   "run",
	Short: "Run http server",
	Run: func(cmd *cobra.Command, args []string) {
		cfg, err := config.Load()
		if err != nil {
			logrus.WithError(err).Error("failed load config")
		}

		logrus.SetLevel(logrus.Level(cfg.Logger.Level))

		db, err := databases.NewPostgresDB(cfg.Postgresql.GetDsn())
		if err != nil {
			logrus.WithError(err).Error("postgresql db connection failed")
		}
		defer db.Close()

		router := rest.NewRouter(db)

		server := rest.NewServer(cfg.Server, router)

		server.Start()
	},
}
