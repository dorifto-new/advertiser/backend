package main

import (
	"advertiser/cmd"
	"log"
)

func main() {
	cmd.RootCmd.AddCommand(cmd.RunCmd, cmd.MigrateCmd)

	if err := cmd.RootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}
