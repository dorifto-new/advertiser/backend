package postgresql

import (
	"advertiser/domain/advertiser"
	"advertiser/internal/databases"
	"database/sql"
	"github.com/jmoiron/sqlx"
)

type AdvertiserRepository struct {
	conn *sqlx.DB
}

func NewAdvertiserRepository(db *sqlx.DB) advertiser.AdvertiserRepository {
	return &AdvertiserRepository{
		conn: db,
	}
}

func (r *AdvertiserRepository) GetAll() ([]advertiser.Advertiser, error) {
	var data []advertiser.Advertiser

	err := r.conn.Select(&data, "SELECT * FROM advertisers")
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (r *AdvertiserRepository) GetByID(id int) (*advertiser.Advertiser, error) {
	var data advertiser.Advertiser

	err := r.conn.Get(&data, "SELECT * FROM advertisers WHERE id = $1", id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, databases.ErrNoRows
		}

		return nil, err
	}

	return &data, nil
}
