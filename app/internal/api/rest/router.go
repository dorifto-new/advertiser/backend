package rest

import (
	"advertiser/domain/advertiser"
	"advertiser/internal/api/rest/handlers"
	postgresqlRepo "advertiser/internal/repositories/postgresql"
	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
)

func NewRouter(db *sqlx.DB) *gin.Engine {
	// Repos
	advertiserRepo := postgresqlRepo.NewAdvertiserRepository(db)

	// Services
	advertiserService := advertiser.NewService(advertiserRepo)

	// Handlers
	advertiserHandler := handlers.NewArticleHandler(advertiserService)

	router := gin.New()
	apiV1 := router.Group("/api/v1/")
	apiV1.GET("/advertisers", advertiserHandler.GetAll)
	apiV1.GET("/advertisers/:id", advertiserHandler.GetByID)

	return router
}
