package rest

import (
	"advertiser/config"
	"fmt"
	"github.com/sirupsen/logrus"
	"net/http"
)

type HttpServer struct {
	http.Server
}

func NewServer(cfg config.Server, handler http.Handler) *HttpServer {
	return &HttpServer{
		Server: http.Server{
			Addr:    fmt.Sprintf("%s:%s", cfg.Address, cfg.Port),
			Handler: handler,
		},
	}
}

func (s HttpServer) Start() {
	if err := s.Server.ListenAndServe(); err != nil {
		logrus.WithError(err).Error("http server error: %s", err.Error())
	}
}
