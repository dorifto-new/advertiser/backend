package handlers

import (
	"advertiser/domain/advertiser"
	"advertiser/internal/api/rest/responses"
	"advertiser/internal/databases"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"net/http"
	"strconv"
)

type AdvertiserHandler struct {
	Service advertiser.AdvertiserService
}

func NewArticleHandler(s advertiser.AdvertiserService) *AdvertiserHandler {
	return &AdvertiserHandler{
		Service: s,
	}
}

func (h AdvertiserHandler) GetAll(c *gin.Context) {
	data, err := h.Service.GetAll()
	if err != nil {
		responses.NewInternalServerError(c)
		return
	}

	logrus.WithError(err).Error("failed to get advertisers list")
	c.JSON(http.StatusOK, responses.AdvertiserListResponse{
		Advertisers: data,
	})
}

func (h AdvertiserHandler) GetByID(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		logrus.WithError(err).Error("failed to parse param")
		responses.NewInternalServerError(c)
		return
	}

	data, err := h.Service.GetByID(id)
	if err != nil {
		if errors.Is(err, databases.ErrNoRows) {
			responses.NewNotFoundError(c)
			return
		}

		logrus.WithError(err).Error("failed to get advertiser")
		responses.NewInternalServerError(c)
		return
	}

	c.JSON(http.StatusOK, responses.AdvertiserResponse{Advertiser: data})
}
