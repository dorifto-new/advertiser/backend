package responses

import "advertiser/domain/advertiser"

type AdvertiserListResponse struct {
	Advertisers []advertiser.Advertiser `json:"data"`
}

type AdvertiserResponse struct {
	Advertiser *advertiser.Advertiser `json:"data"`
}
