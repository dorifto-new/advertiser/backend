package responses

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type Error struct {
	Message string `json:"message"`
}

func NewInternalServerError(c *gin.Context) {
	c.AbortWithStatusJSON(http.StatusInternalServerError, Error{
		Message: "Internal server error",
	})
}

func NewNotFoundError(c *gin.Context) {
	c.AbortWithStatusJSON(http.StatusNotFound, Error{
		Message: "Not found",
	})
}
