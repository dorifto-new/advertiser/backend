package databases

import "errors"

var ErrNoRows = errors.New("no rows in result")

var ErrInternalError = errors.New("internal server error")
